require 'pry'
require 'json'
require 'open3'

class Aids
  attr_accessor :aids
  attr_accessor :根目录

  def initialize(**params)
    self.根目录 = "/home/ubuntu/2T/space/资源体系/视频文件/流媒体平台/B站"
    self.aids = {}
  end

  def 读取对象
    return self.aids unless self.aids.length == 0

    file = File::open("#{self.根目录}/视频/Aids/视频.json")
    self.aids = JSON.parse(file.read)
    file.close

    return self.aids 
  end

  def 写入对象
    return [false, 'aids为空'] if self.aids.nil?

    file = File::open("#{self.根目录}/视频/Aids/视频.json", 'w')
    file.write(JSON.pretty_generate(self.aids))
    file.close
    return [true, ''] 
  end

  def 更新对象文件
    Dir.glob("#{self.根目录}/**/视频.json").each do |文件路径|
      next if /.*\/视频\/.*/.match(文件路径)
        file = File::open(文件路径)
        obj = JSON.parse(file.read)
        file.close
        self.aids.merge!(obj)
    end
    
    self.写入对象
  end

  def 创建软链接
    file = File::open("#{self.根目录}/视频/Aids/视频.json")
    self.aids = JSON.parse(file.read)
    file.close
    
    Dir.glob("#{self.根目录}/视频/Aids/*").each do |文件路径|
      next unless /^.*\.(mp4|flv)$/.match(文件路径)
      `unlink #{文件路径}`
      binding.pry unless $?.success?
    end

    self.aids.each do |aid, detail|
      dir_path = [self.根目录, detail['标签']['本地路径']].flatten.join('/')
      filename = [(detail['文件名']||detail['标题']), detail['文件后缀']].join('.')
      next if filename.include?('已失效')
      next if detail['未下载']
      full_path = [dir_path, filename].join('/')

      # binding.pry unless File::file?(full_path)
      `test -f "#{full_path}"`
      # binding.pry unless $?.success?
      next unless $?.success?

      `ln -s -f "#{full_path}" "#{self.根目录}/视频/Aids/#{aid}.#{detail['文件后缀']}"`
      binding.pry unless $?.success?
    end
  end

  def 文件信息补充
    self.读取对象

    全部文件数量 = Dir.foreach("#{self.根目录}/视频/Aids").to_a.length
    当前计数 = 0

    Dir.foreach("#{self.根目录}/视频/Aids") do |filename|
      当前计数 += 1
      next if /^\.|\.\.$/.match(filename)
      next if /.*[\.json|\.rb]$/.match(filename)
      path = "#{self.根目录}/视频/Aids/#{filename}"
      next unless File::file?(path)

      if (当前计数%1000 == 0)
        # 当前运行账户需配置sudo免密
        puts "清除缓存页"
        `sudo sysctl vm.drop_caches=1`
      end

      aid,file_suffix = filename.split('.')

      puts "[#{Time.now}] [#{当前计数}/#{全部文件数量}] 开始读取: #{path}"

      stdout, stderr, status = Open3.capture3("ffprobe -v quiet -print_format json -show_format -show_streams #{path}")

      unless status.success?
        (self.aids[aid]['异常']||{})['文件信息补充'] = {
          "时间": Time.now,
          "异常信息": stderr,
          "状态码": status.exitstatus
        }
        next
      end

      stdout = JSON.parse(stdout)
      format_names = stdout['format']['format_name'].split(',')
      binding.pry unless self.aids[aid]
      self.aids[aid]['文件可用格式'] = format_names
      self.aids[aid]['文件实际后缀'] = file_suffix
    end

    self.写入对象
  end

end

if __FILE__ == $0
  aids = Aids.new
  aids.更新对象文件
  aids.创建软链接
  aids.文件信息补充
end

require 'pry'
require 'json'

require_relative '../../../Bili/Bili.rb'
BILI = Bili.new()

class CheckFileSuffix
  attr_accessor :根目录
  attr_accessor :对象

  def initialize(**params)
    self.根目录 = BILI.根目录
    self.对象 = {}
  end

  def 写入对象
    return [false, '对象为空'] if self.对象.nil?
    file = File::open("#{self.根目录}/视频/分析/文件后缀错误/视频.json", 'w')
    file.write(JSON.pretty_generate(self.对象))
    file.close
    return [true, ''] 
  end

  def 检查
    AIDS.读取对象.each do |id, info|
      next if ((info['文件可用格式']||[]).length == 0)
      next if info['文件可用格式'].include?(info['文件后缀'])
      self.对象[id] = info
    end
    
    self.写入对象
  end

end

cfs = CheckFileSuffix.new
cfs.检查
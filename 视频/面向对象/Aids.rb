# UP用于指代创作者, USER用于指代普通用户
require File.expand_path("/home/ubuntu/2T/space/资源体系/视频文件/流媒体平台/B站/视频/面向对象/文件夹.rb", __FILE__)

module BiliBili
  module Aids
    extend BiliBili::ExtendMethods

    class Aids < FileSystem::Folder
      attr_accessor :_childrens

      def get
        return self._childrens if self._childrens

        self._childrens = []
        if self.文件对象.json
          self.文件对象.内容 = {}
          self.文件对象.保存
        end

        BiliBili::Dirs.childrens().each do |children|
          self._childrens << children
          (self.文件对象.内容.merge!(children.文件对象.json) rescue binding.pry)
        end

        self.文件对象.保存
        return self._childrens
      end

      def update
        Dir.glob("#{self.绝对路径}/*").each do |文件路径|
          next unless /^.*\.(mp4|flv|#{BILI.默认视频格式})$/.match(文件路径)
          `unlink #{文件路径}`
          binding.pry unless $?.success?
        end

        self.文件对象.json.each do |aid, info|
          next unless info['文件后缀'] == BILI.默认视频格式
          next unless (本地路径=(info['标签']||{})['本地路径'])
          next unless info['文件名']
          链接路径 = [self.绝对路径, "#{aid}.#{BILI.默认视频格式}"].join('/')
          next if File::file?(链接路径)
          文件绝对路径 = [ROOT_PATH, 本地路径, "#{info['文件名']}.#{BILI.默认视频格式}"].flatten.join('/')
          next unless File::file?(文件绝对路径)
          
          # 这里可以不链接直接读源文件格式
          `ln -s -f "#{文件绝对路径}" "#{链接路径}"`
          binding.pry unless $?.success?
        end

        全部文件数量 = Dir.foreach(self.绝对路径).to_a.length
        当前计数 = 0
    
        Dir.foreach(self.绝对路径) do |filename|
          当前计数 += 1
          next if /^\.|\.\.$/.match(filename)
          next if /.*[\.json|\.rb]$/.match(filename)
          path = "#{self.绝对路径}/#{filename}"
          next unless File::file?(path)
    
          if (当前计数%1000 == 0)
            # 当前运行账户需配置sudo免密
            puts "清除缓存页"
            `sudo sysctl vm.drop_caches=1`
          end
    
          aid,file_suffix = filename.split('.')
    
          puts "[#{Time.now}] [#{当前计数}/#{全部文件数量}] 开始读取: #{path}"
    
          stdout, stderr, status = Open3.capture3("ffprobe -v quiet -print_format json -show_format -show_streams #{path}")
    
          unless status.success?
            (self.文件对象.内容[aid]['异常']||{})['文件信息补充'] = {
              "时间": Time.now,
              "异常信息": stderr,
              "状态码": status.exitstatus
            }
            next
          end
    
          stdout = JSON.parse(stdout)
          format_names = stdout['format']['format_name'].split(',')

          binding.pry unless self.文件对象.内容[aid]
          self.文件对象.内容[aid]['文件可用格式'] = format_names
          self.文件对象.内容[aid]['文件实际后缀'] = file_suffix
        end

        self.文件对象.保存    
      end
    end

    def self.childrens
      aids = BiliBili::Aids::Aids.new(绝对路径: File::join(ROOT_PATH, "视频/Aids"), 对象文件名称: "视频.json")
      return [aids].flatten()
    end
    
  end
end

if __FILE__ == $0
  BiliBili::Aids.get()
  BiliBili::Aids.update()
end

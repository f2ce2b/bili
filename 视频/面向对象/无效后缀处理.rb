require File.expand_path("/home/ubuntu/2T/space/资源体系/视频文件/流媒体平台/B站/视频/面向对象/文件夹.rb", __FILE__)


BiliBili::Dirs.childrens().each do |children|
  
  children.文件对象.json.each do |aid, info|
    next unless info['文件后缀'] == 'mp4'
    next unless (本地路径=(info['标签']||{})['本地路径'])
    next unless info['文件名']

    文件夹绝对路径 = [ROOT_PATH, 本地路径].flatten.join('/')
    可能重复路径 = []
    可能重复因素 = [[info['文件名'], info['标题']],['flv', '未知']]
    可能重复因素[0].each do |名称|
      可能重复因素[1].each do |后缀|
        可能重复路径 << [文件夹绝对路径, "#{名称}.#{后缀}"].join('/')
      end
    end

    可能重复路径 = 可能重复路径.uniq

    正确路径 = [文件夹绝对路径, "#{info['文件名']}.mp4"].join('/')

    可能重复路径.each do |路径|
      next unless File::file?(路径)

      if (File::file?(正确路径))
        cmd = %Q|rm -f "#{路径}"|
        print "#{cmd}\n"
        `#{cmd}`
        binding.pry unless $?.success?
      else
        cmd = %Q|mv -vn "#{路径}" "#{正确路径}"|
        print "#{cmd}\n"
        `#{cmd}`
        binding.pry unless $?.success?
      end
    end

  end
end
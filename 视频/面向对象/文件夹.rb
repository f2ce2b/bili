# UP用于指代创作者, USER用于指代普通用户

ROOT_PATH = "/home/ubuntu/2T/space/资源体系/视频文件/流媒体平台/B站"
require File.expand_path("/home/ubuntu/kun20/编程语言/Ruby/文件系统/文件系统.rb", __FILE__)

require File.expand_path(ROOT_PATH + "/Bili/Bili.rb", __FILE__)
BILI = Bili.new()

module BiliBili
  
  # 类方法
  module ExtendMethods
    attr_accessor :_childrens

    def method_missing(method_name, **args)
      res = self.childrens.each do |object|
        break [false, "未找到#{method_name}相关定义方法"] unless object.respond_to?(method_name)

        if args.empty?
          res = object.send(method_name) 
        else
          res = object.send(method_name, args) 
        end

        break res unless res[0]
      end

      return res if res.is_a?(Array) && !res[0]
      return [true, '']
    end
  end

end

module BiliBili
  module Dirs
    class Favorite < FileSystem::Folder
    end

    class User < FileSystem::Folders
      attr_accessor :_childrens

      def childrens
        return self._childrens if self._childrens
        self._childrens = []

        self.子文件夹 do |文件夹|
          self._childrens << Favorite.new(绝对路径: 文件夹.绝对路径, 对象文件名称: "视频.json")
        end

        return self._childrens
      end
    end

    class Favorites < FileSystem::Folders
      attr_accessor :_childrens

      def childrens
        return @_childrens if @_childrens
        @_childrens = []

        self.子文件夹 do |文件夹|
          @_childrens << User.new(绝对路径: 文件夹.绝对路径).childrens
        end

        return @_childrens
      end

    end

    class Up < FileSystem::Folder
    end

    class Ups < FileSystem::Folders
      attr_accessor :_childrens

      def childrens
        return @_childrens if @_childrens
        @_childrens = []

        self.子文件夹 do |文件夹|
          @_childrens << Up.new(绝对路径: 文件夹.绝对路径, 对象文件名称: "视频.json")
        end

        return @_childrens
      end

    end

    def self.childrens(**args)
      ups = BiliBili::Dirs::Ups.new(绝对路径: File::join(ROOT_PATH, "投稿视频"))
      favorites = BiliBili::Dirs::Favorites.new(绝对路径: File::join(ROOT_PATH, "收藏夹"))
      all = [ups.childrens, favorites.childrens].flatten()
      return all 
    end

  end
end

if __FILE__ == $0
  # BiliBili::Dirs.childrens()
end

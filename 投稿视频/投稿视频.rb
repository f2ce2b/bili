require_relative '../Bili/Bili.rb'

class AuthorVideo < Videos

  def get ()
    res = super(接口链接: "http://api.bilibili.com/x/space/arc/search?mid=#{self.用户唯一编码}&ps=50&pn=")
    return res
  end

end

class AuthorVideos < Bili
  def initialize(**params)
    super(params.merge!(所属目录: ['投稿视频']))
    self.bili['投稿视频'].each do |dirname, info|
      self.投稿视频[dirname] = AuthorVideo.new(self.symbolize_keys(info).merge!({所属目录: ['投稿视频',dirname]}))
    end
  end

  def method_missing(method_name, **args)

    res = self.投稿视频.values.each do |object|
      break [false, "未找到#{method_name}相关定义方法"] unless object.respond_to?(method_name)

      if args.empty?
        res = object.send(method_name) 
      else
        res = object.send(method_name, args) 
      end

      break res unless res[0]
    end

    return res if res.is_a?(Array) && !res[0]
    return [true, '']
  end

end

if __FILE__ == $0

  case Dir.pwd.split('/')[-1]
  when 'B站'
    object_file = "#{Dir.pwd}/Bili/Bili.json"
  else
    object_file = "#{Dir.pwd}/../Bili/Bili.json"
  end

  author_videos = AuthorVideos.new(对象文件: object_file)
  ARGV = ['get', 'update', 'download'] if ARGV.empty?
  
  if ARGV.include?('get')
    res = author_videos.get() 
    puts res[1]; exit 255 unless res[0]
  end

  if ARGV.include?('update')
    res = author_videos.update()
    puts res[1]; exit 255 unless res[0]
  end

  if ARGV.include?('download')
    res = author_videos.download()
    puts res[1]; exit 255 unless res[0]
  end

  puts '[true, ""]'
end
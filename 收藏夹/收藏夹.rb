require_relative '../Bili/Bili.rb'

class Favorite < Videos

  def get ()
    res = super(接口链接: "https://api.bilibili.com/x/v3/fav/resource/list?media_id=#{self.收藏夹唯一编码}&ps=20&pn=")
    return res
  end
end

class Favorites < Bili

  def initialize(**params)
    super(params.merge!(所属目录: ['收藏夹']))
    self.bili['收藏夹'].each do |user_name, infos|
      self.收藏夹[user_name] = {}
      infos['列表'].each do |dirname, info|
        self.收藏夹[user_name][dirname] = Favorite.new(self.symbolize_keys(info).merge!({所属目录: ['收藏夹',user_name, dirname]}))
      end
    end
  end

  def method_missing(method_name, **args)
    res = self.收藏夹.values.map(&:values).flatten.each do |object|
      break [false, "未找到#{method_name}相关定义方法"] unless object.respond_to?(method_name)

      if args.empty?
        res = object.send(method_name) 
      else
        res = object.send(method_name, args) 
      end

      break res unless res[0]
    end

    return res if res.is_a?(Array) && !res[0]
    return [true, '']
  end

  # 更新用户收藏夹
  # 需要个人空间隐私设置中公开显示我的收藏
  def self.update(对象文件: nil)
    bili = JSON.parse(File.read(对象文件)).clone

    res = bili['收藏夹'].each do |user_name, infos|
      api = Api.new(接口链接: "http://api.bilibili.com/x/v3/fav/folder/created/list-all?up_mid=#{infos['用户唯一编码']}", cookie: bili['cookie'])

      res = api.request
      break res unless res[0]
  
      result = Result.new(请求对象: res[2], 响应结果: res[1])
  
      res = result.format()
      break res unless res[0] 
      
      details = res[1]['列表']

      (infos['列表']||={}).each do |dirname, info|
        detail = details[info['收藏夹唯一编码']]
        next unless detail
        detail[:允许下载的最大时长] ||= '不限'
        infos['列表'][dirname] = detail
      end

      favids = infos['列表'].values.map{|hash| hash[:收藏夹唯一编码]}
      # details.delete_if{|key, value| favids.include?(value[:收藏夹唯一编码])}

      details.each do |key, value|
        infos['列表'][value[:名称]] = value
      end

      infos['列表'].each do |dirname, info|
        dirname = dirname.to_s
        dir_path = lambda{|dirname| return File.join(bili['全局参数']['根目录'], ['收藏夹', user_name, dirname]) }
        if (dirname == info[:名称])
          unless Dir.exist?(dir_path.call(dirname))
            `mkdir "#{dir_path.call(dirname)}"`
            `echo '{}' > "#{dir_path.call(dirname)}/视频.json"`
          end
          next
        end

        if Dir.exist?(dir_path.call(dirname))
          `mv "#{dir_path.call(dirname)}" "#{dir_path.call(info[:名称])}"`
          infos['列表'].delete_if{|key, value| key.to_s == dirname }
          # infos['列表'][info[:名称].to_sym] = info
          # bili['收藏夹'][user_name]['列表'][info[:名称]] = info
        else
          infos['列表'].delete_if{|key, value| key.to_s == dirname }
        end

      end

    end

    return res if res.is_a?(Array) && !res[0]

    file = File::open(对象文件, mode='w+')
    file.write(JSON.pretty_generate(bili))
    file.close
    return [true, '']
  end

end

if __FILE__ == $0
  case Dir.pwd.split('/')[-1]
  when 'B站'
    object_file = "#{Dir.pwd}/Bili/Bili.json"
  else
    object_file = "#{Dir.pwd}/../Bili/Bili.json"
  end

  Favorites.update(对象文件: object_file)
  favorites = Favorites.new(对象文件: object_file)
  ARGV = ['get', 'update', 'download'] if ARGV.empty?
  
  if ARGV.include?('get')
    res = favorites.get() 
    puts res[1]; exit 255 unless res[0]
  end

  if ARGV.include?('update')
    res = favorites.update()
    puts res[1]; exit 255 unless res[0]
  end

  if ARGV.include?('download')
    res = favorites.download()
    puts res[1]; exit 255 unless res[0]
  end

  puts '[true, ""]'
end
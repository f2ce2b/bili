
requires = {
  'pry': '断点调试',
  'rest-client': 'Http请求', # gem install rest-client -v 1.8.0
  'json': 'JSON数据处理',
  'ostruct': 'JSON转Ruby对象',
}

requires.each do |gem_name, desc|
  begin 
    require gem_name.to_s
  rescue LoadError => e
    puts "缺少Gem: #{gem_name}(用于#{desc}), 请先手动安装该Gem."
  end
end

require_relative '../视频/Aids/Aids.rb'
AIDS = Aids.new

class Core
  def attributes
    hash = self.instance_variables.each_with_object({}) do |var, hash| 
      hash[var.to_s.delete("@")] = self.instance_variable_get(var)
    end

    hash
  end

  def symbolize_keys(obj)
    case obj
    when Array
      obj.inject([]) { |res, val|
        res << case val
        when Hash, Array
          symbolize_keys(val)
        else
          val
        end
        res
      }
    when Hash
      obj.inject({}) { |res, (key, val)|
        nkey = case key
          when String
            key.to_sym
          else
            key
          end
        nval = case val
          when Hash, Array
            symbolize_keys(val)
          else
            val
          end
        res[nkey] = nval
        res
      }
    else
      obj
    end
  end
end

class Bili < Core
  attr_accessor :bili
  attr_accessor :所属目录

  def initialize(params_={}, **params)
    params = params_.merge!(params) unless params_.empty?
    params = self.symbolize_keys(params)

    # bili_json = '/home/ubuntu/2T/space/资源体系/视频文件/流媒体平台/B站/Bili/Bili.json'
    bili_json = (params[:对象文件] || File.join(File.dirname(__FILE__), 'Bili.json'))
    self.bili = JSON.parse(((File.read(bili_json) rescue binding.pry) || '{}'))

    deep_value = lambda do |object, keys|
      return object if keys.length == 0
      object = (object[keys[0]]||{})
      keys.delete_at(0)
      deep_value.call(object, (keys||[]))
    end

    this = {}
    this.merge!((self.bili['全局参数']||{}).clone)
    deep_values = deep_value.call(self.bili, (params[:所属目录]||=[]).clone)
    this.merge!(deep_values.clone)
    this.merge!(params)

    this.each do |key, value|
      self.class.attr_accessor key
      self.instance_variable_set("@#{key}", value)
    end

    unless self.所属目录.empty?
      self.class.attr_accessor self.所属目录[-1]
      self.instance_variable_set("@#{self.所属目录[-1]}", {})
    end    
  end

  def private_attributes()
    public_keys = ['bili', '所属目录', self.bili.keys, self.bili['全局参数'].keys].flatten.map(&:to_s)
    return self.attributes.delete_if{|key, value| public_keys.include?(key.to_s)}
  end

end

class Api < Bili
  attr_accessor :接口链接
  attr_accessor :接口参数
  attr_accessor :请求类型
  attr_accessor :请求头
  attr_accessor :请求体
  attr_accessor :cookie

  def initialize(**params)
    super(params)
    self.请求类型 ||= 'Get'
    self.请求头 ||= {}
    self.请求体 ||= {}
    self.cookie ||= {}

    if self.cookie.is_a?(String)
      self.cookie = Hash[self.cookie.split('; ').map{|item| item.split('=')}]
    end

  end

  def request ()
    params = {method: self.请求类型, headers: self.请求头, url: self.接口链接, payload: self.请求体, cookies: self.cookie}

    if self.全局参数['网络代理'] && self.全局参数['网络代理']['是否启用'] && (self.全局参数['网络代理']['代理池']||[]).length > 0
      params[:proxy] = self.全局参数['网络代理']['代理池'].sample
    end

    puts "#{Time.now} 请求: #{JSON.pretty_generate(params)}"
    res = RestClient::Request.execute(params)
    puts "#{Time.now} 返回结果: #{res.body}"
    return [(res.code == 200), res.body, res]
  end

end

class Result < Bili
  attr_accessor :请求对象
  attr_accessor :响应结果
  attr_accessor :格式化结果

  def format()
    json = JSON.parse(self.响应结果, object_class: OpenStruct)
    
    # if json.code && json.code != 0
    #   # return [false, '访问异常', self.响应结果]
    #   puts "\n"+JSON.pretty_generate(JSON.parse(self.响应结果))
    #   print "\n#{json.message || '访问异常'}\n"
    #   exit 255
    # end

    data = json['data']
    return [false, '预期结果为空', self.响应结果] unless data

    self.格式化结果 = {}

    case self.请求对象.request.url
    # 视频播放链接
    when /.*player\/playurl.*/
      self.格式化结果['视频链接'] = data['durl'].map{|item| item['url']}
    # 收藏夹收藏视频
    when /.*fav\/resource.*/
      return [false, '目标结果为空', self.响应结果] unless data['medias']

      self.格式化结果['视频详情'] = {}

      data.medias.each do |media|
        formatted = {}
        formatted['cid'] = media.ugc.first_cid
        formatted['bvid'] = media.bvid
        formatted['链接'] = 'https://www.bilibili.com/video/' + media.bvid
        formatted['标题'] = media.title
        formatted['持续时长'] = media.duration
        formatted['作者'] = media.upper.name
        formatted['作者唯一编码'] = media.upper.mid
        formatted['发布时间'] = {
          "数字": media.pubtime,
          "字符串": Time.at(media.pubtime).to_s
        }
        self.格式化结果['视频详情'][media.id.to_s] = formatted
      end
    # 用户投稿视频
    when /.*space\/arc\/search.*/
      return [false, '目标对象为空', self.响应结果] unless (data['list']||{})['vlist']

      self.格式化结果['视频详情'] = {}

      data.list.vlist.each do |media|
        formatted = {}
        formatted['cid'] = '暂无'
        formatted['bvid'] = media.bvid
        formatted['链接'] = 'https://www.bilibili.com/video/' + media.bvid
        formatted['标题'] = media.title
        formatted['持续时长'] = media.length
        formatted['作者'] = media.author
        formatted['作者唯一编码'] = media.mid
        formatted['发布时间'] = {
          "数字": media.created,
          "字符串": Time.at(media.created).to_s
        }
        self.格式化结果['视频详情'][media.aid.to_s] = formatted
      end

    # 根据bvid获取cid
    when /.*player\/pagelist.*/
      self.格式化结果['cids'] = data.map{|json| json['cid']}.join('/')
    # 根据bvid获取视频详情
    when /x\/web\-interface\/view/
      if data.pages
        self.格式化结果['cids'] = data.pages.map(&:cid).join('/')
      else
        self.格式化结果['cids'] = "无法获取"
      end

      self.格式化结果['发布时间'] = {
        "数字": data.pubdate,
        "字符串": Time.at(data.pubdate).to_s
      }

    # 用户创建的视频收藏夹
    when /.*fav\/folder\/created\/list\-all.*/
      self.格式化结果['列表'] = {}
      data.list.each do |favorite|
        self.格式化结果['列表'][favorite.id] = {
          "收藏夹唯一编码": favorite.id,
          "用户唯一编码": favorite.mid,
          "名称": favorite.title,
          "视频数量": favorite.media_count,
        }
      end
    else
      return [false, '无对应处理方法', self.响应结果]
    end

    return [true, self.格式化结果]
  end

end

class Download < Bili
  attr_accessor :存储文件夹
  attr_accessor :存储名称
  attr_accessor :下载链接
  attr_accessor :重试次数

  def initialize(**params)
    super(params)
    self.重试次数 ||= 10
  end

  def save()
    return [true, '文件已存在'] if File::file?("#{self.存储文件夹}/#{self.存储名称}")

    begin
      `mkdir -p "#{self.存储文件夹}"`
      `wget -t #{self.重试次数} -c -O "#{self.存储文件夹}/#{self.存储名称}" --referer 'https://www.bilibili.com' "#{self.下载链接}"`
      # binding.pry unless $?.success?
      
      # stdout, stderr, status = Open3.capture3("wget -t #{self.重试次数} -c -O '#{self.存储文件夹}/#{self.存储名称}' --referer 'https://www.bilibili.com' #{self.下载链接}")
      # $?.exitstatus
      # $?.success?
    rescue => e
      binding.pry
    end

    return [true, '']

  end
end

class Video < Bili
  attr_accessor :aid
  attr_accessor :cid
  attr_accessor :bvid
  attr_accessor :存储文件夹
  attr_accessor :存储名称
  attr_accessor :发布时间
  attr_accessor :标签
  attr_accessor :文件后缀
  attr_accessor :文件名

  def download()
    self.存储文件夹 ||= File.join(self.根目录, self.标签[:本地路径])
    self.存储名称 ||= "#{self.文件名}.#{self.文件后缀}"
    return [true, '文件已存在'] if File::file?("#{self.存储文件夹}/#{self.存储名称}")

    api = Api.new(接口链接: "http://api.bilibili.com/x/player/playurl?bvid=#{self.bvid}&cid=#{self.cid}&qn=80&fnver=0&fnval=0&fourk=1", cookie: self.cookie)

    res = api.request

    binding.pry unless res[0]

    result = Result.new(请求对象: res[2], 响应结果: res[1])

    res = result.format()

    return res unless res[0]

    binding.pry if res[1]['视频链接'].size > 1

    res[1]['视频链接'].each do |url|
      download = Download.new(下载链接: url, 存储文件夹: self.存储文件夹, 存储名称: self.存储名称)
      res = download.save()

      binding.pry unless res[0]
      next if res[1] == '文件已存在'
      
      sleep self.请求间隔
    end

    return [true, '']
  end

  def 文件已下载?(文件后缀: self.文件后缀)
    return false unless self.标签
    存储文件夹 = File.join(self.根目录, self.标签[:本地路径])
    return false unless File::file?("#{存储文件夹}/#{self.文件名||self.标题}.#{文件后缀}")
    return true
  end

  def 文件重命名(新文件名:, 原始文件名:)
    存储文件夹 = File.join(self.根目录, self.标签[:本地路径])
    return [false, '原始文件不存在' ] unless File::file?("#{存储文件夹}/#{原始文件名}")
    
    if (File::file?("#{存储文件夹}/#{新文件名}"))
      cmd = %Q|rm -f "#{存储文件夹}/#{原始文件名}"|
      print cmd
      `#{cmd}`
      binding.pry unless $?.success?
    else
      cmd = %Q|mv "#{存储文件夹}/#{原始文件名}" "#{存储文件夹}/#{新文件名}"|
      print cmd
      `#{cmd}`
      binding.pry unless $?.success?
    end

    return [true, '']
  end

  def 更新文件名
    return unless self.文件名.nil?
    原始文件名 = "#{self.文件名||self.标题}.#{self.文件后缀}"
    if self.文件名.nil? && /\/|\||\!/.match(self.标题)
      self.文件名 = self.标题.gsub('/', '／').gsub('|', '｜').gsub('!', '！')
    elsif self.文件名.nil?
      self.文件名 = self.标题
    end
  end

  def 更新持续时长
    return unless self.持续时长.is_a?(String)
    
    case self.持续时长
    when /\d{2,}:\d{2}/
      min, sec = self.持续时长.split(':')
      self.持续时长 = min.to_i*60+sec.to_i
    else
      binding.pry
    end

  end

  def 更新文件后缀
    self.文件后缀 ||= self.默认视频格式
    return unless AIDS.读取对象[self.aid]
    return unless AIDS.读取对象[self.aid]["文件可用格式"]
    文件可用格式 = AIDS.读取对象[self.aid]["文件可用格式"]
    return if 文件可用格式.include?(self.文件后缀)

    case 文件可用格式.join(' ')
    when /.* mp4 .*/
      file_suffix = 'mp4'
    else
      binding.pry
    end

    self.文件后缀 = file_suffix
  end

  def 更新文件格式
    return if self.文件后缀 != self.默认视频格式
    return unless AIDS.读取对象[self.aid]
    return if self.文件后缀 == AIDS.读取对象[self.aid]['文件实际后缀']

    if self.文件已下载?(文件后缀: self.默认视频格式)
      新文件名 = "#{self.文件名}.#{self.文件后缀}"
      原始文件名 = "#{self.文件名}.#{self.默认视频格式}"
      res = 文件重命名(新文件名: 新文件名, 原始文件名: 原始文件名)
      p res[1];exit 255 unless res[0]
    end

  end

  def update()
    need_request_detail = [
      ['暂无', ''].include?(self.cid),
      self.发布时间.nil?
    ]

    if need_request_detail.include?(true)
      api = Api.new(接口链接: "http://api.bilibili.com/x/web-interface/view?bvid=#{self.bvid}", cookie: self.cookie)
      res = api.request
      return res unless res[0]
      result = Result.new(请求对象: res[2], 响应结果: res[1])

      res = result.format()
      return res unless res[0]
      video_detail = res[1]
      sleep self.请求间隔
    end

    if ['暂无', ''].include?(self.cid)
      return [false, '[更新cid] bvid属性为空'] if self.bvid.empty?
      self.cid = video_detail['cids']
    end

    更新持续时长
    更新文件名
    更新文件后缀
    更新文件格式

    self.发布时间 = video_detail['发布时间'] if self.发布时间.nil?
    return [true, self.symbolize_keys(self.private_attributes)]
  end
end

class Videos < Bili
  attr_accessor :视频对象

  def initialize(**params)
    super(params)
    
    unless self.视频对象
      self.视频对象 = (JSON.parse(File.read(File.join(self.根目录, self.所属目录, '视频.json'))) rescue binding.pry)
    end
  end

  def get(接口链接: nil)
    keep_on = true
    page_num = 0

    res = while keep_on
      page_num += 1

      api = Api.new(接口链接: 接口链接+page_num.to_s, cookie: self.cookie)

      res = api.request

      unless res[0]
        keep_on = false
        break res
      end

      result = Result.new(请求对象: res[2], 响应结果: res[1])
      res = result.format()

      if res[0]
        keep_on = !((res[1]['视频详情'].keys - self.视频对象.keys).length == 0)
        self.save(res[1]['视频详情'])
      else
        keep_on = false
        break res unless res[2]
        
        error = (JSON.parse(res[2]) rescue '{}')
        break res unless error['data']
        break [true, ''] if error['data']['has_more'] == false
      end

      sleep self.请求间隔
    end

    return res if res.is_a?(Array) && !res[0]
    return [true, '']
  end

  def save(attributes={})
    # TODO: 这块保存的逻辑需优化
    # self.视频对象 = self.视频对象.merge!(attributes.merge!(self.视频对象))
    self.视频对象 = attributes.merge(self.视频对象) do |key, newval, oldval | 
      # 如果原有对象存在值则用原有的
      newval.merge(oldval) do |k,n,o| 
        o.nil? ? n : o
      end
    end

    file = File::open(File.join(self.根目录, self.所属目录, '视频.json'), mode='w+')
    file.write(JSON.pretty_generate(self.视频对象))
    file.close
  end

  def update()
    return [false, '视频对象不能为空'] unless self.视频对象
    return [false, '视频对象格式错误'] unless self.视频对象.is_a?(Hash)

    res = self.视频对象.each do |aid, info|
      old_info = self.symbolize_keys(info)
      old_info[:aid] ||= aid
      res = Video.new(old_info).update()
      break res unless res[0]
      self.视频对象[aid] = res[1]
      video = self.视频对象[aid]

      time_stamp = (video[:发布时间]||{})[:数字]
      if time_stamp
        year = Time.at(time_stamp).year.to_s
        local_path = self.所属目录.clone.append(year)
        (video[:标签]||={})[:本地路径] = local_path
      end
            
      self.save() unless old_info == video
    end

    return res if res.is_a?(Array) && !res[0]
    return [true, '']
  end

  def download()
    videos_count = self.视频对象.keys.size
    index = 0

    res = self.视频对象.each do |id, item|
      index+=1
      item = self.symbolize_keys(item)
      title = item[:标题]
      next if title == '已失效视频'
      next if item[:未下载]
      
      if item[:cid] == '无法获取'
        self.视频对象[id]['未下载'] = {
          '类型': 'cid异常',
          '详情': "无法获取正确cid"
        }
        self.save()
        next
      end

      if self.允许下载的最大时长 != '不限' && (item[:持续时长] > self.允许下载的最大时长) && !item[:超长视频]
        self.视频对象[id]['未下载'] = {
          '类型': '时长超限',
          '详情': "请检查是否需要下载,如需下载请添加[超长视频]属性"
        }
        self.save()
        next
      end

      puts "#{Time.now} [#{index}/#{videos_count}] 开始下载: #{title}"

      video = Video.new(item)

      download = lambda do |retry_count=0|
        begin
          res = video.download()
          unless res[0]
            self.视频对象[id]['未下载'] = {
              '类型': '下载失败',
              '详情': JSON.parse((res[2] || '{}'))
            }
            self.save()
          end
        rescue RestClient::Exceptions::OpenTimeout, SocketError => e
          if retry_count <= self.重试次数
            sleep self.请求间隔*2
            return download.call(retry_count+1)
          else
            return [false, '网络连接异常']
          end
        rescue => e
          self.视频对象[id]['未下载'] = {
            '类型': '下载异常',
            '详情': e.to_s
          }
          self.save()
        end
        return res
      end

      res = download.call()
      
      # break res unless res[0]
    end

    return res if res.is_a?(Array) && !res[0]
    return [true, '']
  end

end
#!/bin/bash --login

function update(){
  # ruby 视频/Aids/Aids.rb
  ruby 视频/面向对象/Aids.rb
  if [[ ! $? -eq 0 ]];then echo '视频聚合失败'; exit 255;fi

  ruby 收藏夹/收藏夹.rb update
  if [[ ! $? -eq 0 ]];then echo '收藏夹更新执行失败'; exit 255;fi

  ruby 投稿视频/投稿视频.rb update
  if [[ ! $? -eq 0 ]];then echo '投稿视频更新执行失败'; exit 255;fi

  ruby 视频/面向对象/无效后缀处理.rb
  if [[ ! $? -eq 0 ]];then echo '无效后缀处理执行失败'; exit 255;fi

  echo "清除缓存页"
  sudo sysctl vm.drop_caches=1
}

update;

ruby 收藏夹/收藏夹.rb
if [[ ! $? -eq 0 ]];then echo '收藏夹执行失败'; exit 255;fi

update;
git add **/*.json
git add **/**/*.json
git add **/**/**/*.json
git commit -m  "[自动][$(date '+%Y%m%d%H%M%S')]对象文件更新"
git push origin master

ruby 投稿视频/投稿视频.rb
if [[ ! $? -eq 0 ]];then echo '投稿视频执行失败'; exit 255;fi

update;
git add **/*.json
git add **/**/*.json
git add **/**/**/*.json
git commit -m  "[自动][$(date '+%Y%m%d%H%M%S')]对象文件更新"
git push origin master
